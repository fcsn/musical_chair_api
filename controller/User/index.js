const models = require('../../models')
const Joi = require('joi')

const getUsers = async (req, res) => {
    const users = await models.User.findAll()
    // res.body = users
    return res.json(users)
}

const getUser = async (req, res) => {
    const { id } = req.params
    const user = await models.User.findOne({
        where: { id },
        // include: [{
        //     model: models.Project,
        //     as: 'project',
        // }]
    });
    return res.json(user)
};

const makeUser = (req, res, next) => {
    models.User.create({
        name: req.body.name,
        email: req.body.email,
    })
        .then((result) => {
            console.log(result)
            res.status(201).json(result)
        })
        .catch((err) => {
            console.error(err)
            next(err)
        });
}

module.exports = {
    getUser,
    getUsers,
    makeUser
}
