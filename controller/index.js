// const models = require('../models');
const Joi = require('joi')
const axios = require('axios')
const models = require('../models')
const jwt = require('jsonwebtoken')

// const getRoot = (req, res) => res.send('root')
const appKey = '3503d21604417bf05de248d467ca5e22'
const kakaoRedirectUrl = 'http://localhost:3000/oauth'
const kakaoDialogUrl = `https://kauth.kakao.com/oauth/authorize?client_id=${appKey}&redirect_uri=${kakaoRedirectUrl}&response_type=code&scope=account_email`

const getRoot = (req, res) => res.render('index', {
    kakaoUrl: kakaoDialogUrl
})

const getOauth = (req, res) => {
    const authorizeCode = req.query.code
    const tokenEndPoint = `https://kauth.kakao.com/oauth/token?grant_type=authorization_code&client_id=${appKey}&redirect_uri=${kakaoRedirectUrl}&code=${authorizeCode}`

    axios.post(tokenEndPoint).then(response => {
        axios.get(
            'https://kapi.kakao.com/v2/user/me',
            {
                headers: { Authorization: `Bearer ${response.data.access_token}` }
            }
        ).then(response2 => {
            // 카카오 꺼만 볼 때
            // res.json(response2.data)

            // 분기 없이 로그인
            models.User.create({
                providerId: response2.data.id,
                name: response2.data.properties.nickname,
                email: response2.data.kakao_account.email
            })
                .then(result => {
                    console.log(result);
                    // res.status(201).json(result);
                    let token = jwt.sign({
                            id: result.id   // 토큰의 내용(payload)
                        },
                        'secret',  // 비밀 키
                        {
                            expiresIn: '1m'  // 유효 시간은 1분
                        })
                    res.cookie('user', token)
                    res.status(201).json({
                        token: token
                    })
                })
                .catch(err => {
                    console.error(err);
                    next(err);
                });

            // 분기 했을 때
            // const user = models.User.findOne({
            //     where: { providerId: response2.data.id },
            // })
            // if (user) {
            //     models.User.update({
            //         providerId: response2.data.id,
            //         name: response2.data.properties.nickname,
            //         email: response2.data.kakao_account.email
            //     },{
            //         where: { providerId: response2.data.id }
            //     })
            //         .then(result => {
            //             console.log(result);
            //             res.status(201).json(result);
            //         })
            //         .catch( err => {
            //             console.error(err);
            //         });
            // } else {
            //     models.User.create({
            //         providerId: response2.data.id,
            //         name: response2.data.properties.nickname,
            //         email: response2.data.kakao_account.email
            //     })
            //         .then(result => {
            //             console.log(result);
            //             res.status(201).json(result);
            //         })
            //         .catch(err => {
            //             console.error(err);
            //             next(err);
            //         });
            // }
        }).catch((err) => {
            console.log('then error : ', err)
        })
    })

    // token
    // models.User.findOne({
    //     where: {
    //         id: 2
    //     }
    // })
    //     .then(user => {
    //         let token = jwt.sign({
    //                 id: user.id   // 토큰의 내용(payload)
    //             },
    //             'secret',  // 비밀 키
    //             {
    //                 expiresIn: '1m'  // 유효 시간은 1분
    //             })
    //         res.cookie('user', token)
    //         res.json({
    //             token: token
    //         })
    //     })
}

module.exports = {
    getRoot,
    getOauth
}
