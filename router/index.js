const express = require('express')
const jwt = require('jsonwebtoken')
const models = require('../models')

const {
    getRoot, getOauth,
    } = require('../controller')

const {
    getUsers, getUser, makeUser
} = require('../controller/User')

const router = express.Router()

router.get('/', getRoot)
router.get('/oauth', getOauth)



router.get('/login', (req, res, next) => {
    let token = jwt.sign({
            id: 2   // 토큰의 내용(payload)
        },
        'secret',  // 비밀 키
        {
            expiresIn: '1m'  // 유효 시간은 1분
        })


    models.User.findOne({
        where: {
            id: 2
        }
    })
        .then(user => {
                res.cookie('user', token)
                res.json({
                    token: token
                })
        })
})



router.get('/users', getUsers);
router.get('/users/:id', getUser);
router.post('/user', makeUser);

module.exports = router;
