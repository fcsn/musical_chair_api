const express = require('express')
const router = require('./router')
const bodyParser = require('body-parser')

const app = new express()

app.use(bodyParser.json())

app.set('view engine', 'ejs')

app.use('/', router)

module.exports = app;
